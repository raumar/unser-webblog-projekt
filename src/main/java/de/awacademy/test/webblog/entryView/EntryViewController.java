package de.awacademy.test.webblog.entryView;


import de.awacademy.test.webblog.login.LoginDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;


@Controller
public class EntryViewController {

    @Autowired
    private EntryViewService entryViewService;

    @GetMapping("/entryView")
    public String showEntry(@RequestParam("entryId") long entryId, Model model) throws IOException {

        entryViewService.addAttributesToModelForShowEntry(entryId, model);

        return "entry_view";
    }

    @GetMapping("/showEntryHistory")
    public String showEntryHistory(@RequestParam("entryId") long entryId, Model model) throws IOException {

        entryViewService.addAttributesToModelForShowEntry(entryId, model);

        return "entry_history_view";
    }




}
