package de.awacademy.test.webblog.entryView;

import de.awacademy.test.webblog.comment.CommentDTO;
import de.awacademy.test.webblog.comment.CommentRepository;
import de.awacademy.test.webblog.entry.Entry;
import de.awacademy.test.webblog.entry.EntryRepository;
import de.awacademy.test.webblog.entry.EntryService;
import de.awacademy.test.webblog.images.ImageService;
import de.awacademy.test.webblog.login.LoginDTO;
import de.awacademy.test.webblog.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Service
public class EntryViewService {

    @Autowired
    private EntryRepository entryRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private ImageService imageService;


    public void addAttributesToModelForShowEntry(long entryId, Model model) throws IOException {

        Entry entryToView = entryRepository.findFirstById(entryId);

        String pic = imageService.getImage(entryToView.getImage().getId());

        model.addAttribute("pic", pic);
        model.addAttribute("comment", new CommentDTO());
        model.addAttribute("comments", commentRepository.findAllByEntryOrderByCreationDateAsc(entryToView));
        model.addAttribute("entry", entryToView);
        model.addAttribute("login", new LoginDTO());

    }

}
