package de.awacademy.test.webblog.user;

import de.awacademy.test.webblog.entry.EntryRepository;
import de.awacademy.test.webblog.session.SessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class UserController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;

    @GetMapping("/userAdministration")
    public String showUsers(Model model, @ModelAttribute("currentUser") User currentUser) {

        if (!UserService.checkUserforAdmin(currentUser)) {
            return "redirect:/";
        } else {
            model.addAttribute("users", userRepository.findAllByAdministratorEqualsOrderByName(false));
            model.addAttribute("admins", userRepository.findAllByAdministratorEqualsOrderByName(true));
            return "user_administration";
        }
    }

    @PostMapping("/userAdministration")
    public String searchUsers(Model model, @ModelAttribute("currentUser") User currentUser,
                              @RequestParam("userName") String userName) {

        if (!UserService.checkUserforAdmin(currentUser)) {
            return "redirect:/";
        } else {
            model.addAttribute("users", userRepository.findAllByAdministratorEqualsAndNameIgnoreCaseContainingOrderByName(false, userName));
            model.addAttribute("admins", userRepository.findAllByAdministratorEqualsAndNameIgnoreCaseContainingOrderByName(true, userName));
            return "user_administration";
        }
    }


    @PostMapping("/makeAdministrator")
    public String makeAdministrator(Model model, @RequestParam("id") long id, @ModelAttribute("currentUser") User currentUser) {
        if (!UserService.checkUserforAdmin(currentUser)) {
            return "redirect:/";
        } else {
            userService.makeAdmin(id);

            return "redirect:/userAdministration";
        }
    }

}
