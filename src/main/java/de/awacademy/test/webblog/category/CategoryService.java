package de.awacademy.test.webblog.category;


import de.awacademy.test.webblog.entry.Entry;
import de.awacademy.test.webblog.entry.EntryRepository;
import de.awacademy.test.webblog.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
public class CategoryService {


    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    EntryRepository entryRepository;


    public void saveNewCategory(User currentUser, CategoryDTO category) {

        if (currentUser != null && currentUser.isAdministrator()) {

            Category categoryEntity = new Category(category.getNameCategory());
            categoryRepository.save(categoryEntity);
        }


    }

    @Transactional
    public void deleteCategory(User currentUser, Long id) {

        if (currentUser.isAdministrator()) {
            Category toEdit = categoryRepository.getFirstById(id);

            List<Entry> entrys = toEdit.getEntrys();

            for (Entry e : entrys) {
                e.getCategories().remove(toEdit);
                entryRepository.save(e);
            }

            categoryRepository.delete(toEdit);
        }
    }

}
