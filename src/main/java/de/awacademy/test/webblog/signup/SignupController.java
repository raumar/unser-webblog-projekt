package de.awacademy.test.webblog.signup;

import de.awacademy.test.webblog.login.LoginDTO;
import de.awacademy.test.webblog.session.Session;
import de.awacademy.test.webblog.session.SessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Controller
public class SignupController {

    @Autowired
    SessionRepository sessionRepository;
    @Autowired
    SignupService signupService;

    @GetMapping("/signup")
    public String signup(Model model) {

        model.addAttribute("signup", new SignupDTO());
        model.addAttribute("login", new LoginDTO());
        return "signup";
    }

    @PostMapping("/signup")
    public String signup(@ModelAttribute("signup") @Valid SignupDTO signup, BindingResult bindingResult,
                         HttpServletResponse response, Model model) {

        signupService.checkInputForErrors(model, bindingResult, signup);

        if (bindingResult.hasErrors()) {

            model.addAttribute("signup", new SignupDTO());
            model.addAttribute("login", new LoginDTO());

            return "signup";
        }

        Session s = new Session(signupService.saveNewUser(signup));
        sessionRepository.save(s);

        response.addCookie(new Cookie("sessionId", s.getId()));
        return "redirect:/";
    }
}
