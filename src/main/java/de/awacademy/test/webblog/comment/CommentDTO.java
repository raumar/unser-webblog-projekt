package de.awacademy.test.webblog.comment;



import de.awacademy.test.webblog.entry.Entry;

import javax.validation.constraints.Size;

public class CommentDTO {

    @Size(min = 4)
    private String text;

    private Entry entry;




    public Entry getEntry() {
        return entry;
    }

    public void setEntry(Entry entry) {
        this.entry = entry;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
