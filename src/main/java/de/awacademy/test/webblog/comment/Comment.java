package de.awacademy.test.webblog.comment;

import de.awacademy.test.webblog.entry.Entry;
import de.awacademy.test.webblog.user.User;

import javax.persistence.*;
import java.time.Instant;

@Entity
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Lob
    private String text;

    @ManyToOne
    private User user;

    @ManyToOne
    private Entry entry;

    private Instant creationDate;


    public Comment() {
    }

    public Comment( String text, User user, Entry entry) {

        this.entry = entry;
        this.text = text;
        this.user = user;
        this.creationDate = Instant.now();
    }


    public long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public User getUser() {
        return user;
    }

    public Instant getCreationDate() {
        return creationDate;
    }
}
