package de.awacademy.test.webblog.comment;


import de.awacademy.test.webblog.entry.Entry;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface CommentRepository extends JpaRepository<Comment, Long> {

    List<Comment> findAllByEntryOrderByCreationDateAsc(Entry entry);

    Comment findFirstById(Long id);

    void deleteById(Long id);

    void deleteByEntry(Entry entry);

}
