package de.awacademy.test.webblog.entryVersion;

import de.awacademy.test.webblog.entry.Entry;
import de.awacademy.test.webblog.user.User;

import javax.persistence.*;
import java.time.Instant;

@Entity
public class EntryVersion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String title;


    @Lob
    private String text;

    private Instant changeDate;

    @ManyToOne
    private Entry entry;

    @ManyToOne
    private User user;

    public EntryVersion() {
    }

    public EntryVersion(String title, String text, Entry entry, User changer) {
        this.title = title;
        this.text = text;
        this.entry = entry;
        this.user = changer;
        this.changeDate = Instant.now();
    }

    public User getUser() {
        return user;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public Instant getChangeDate() {
        return changeDate;
    }

    public Entry getEntry() {
        return entry;
    }
}
